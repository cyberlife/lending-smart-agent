const cyberLoanCreator = require("./loan/askForCyberLoan.js")
const minimist = require('minimist');
var base = require('cyber-base')

function agentFunc(paramTypes) {

  this.paramTypes = paramTypes

}

var agentDictionary = {

  "createCollateralizedLoan": new agentFunc(["number", "number"]),
  "createArt": new agentFunc([])

}

//------------------------------------------------------------------

const argv = minimist(process.argv);
let pf = argv._[argv._.length - 2];
let p = argv._[argv._.length - 1];


async function createCollateralizedLoan(minEthPrice, ethToSend) {

  //var cdpData = await maker.createCDP(minEthPrice, ethToSend);

  //await maker.topupCDP(cdpData[0]);

  console.log(`CREATING A CDP\n\n`)

  //Declare that the smart agent is starting its work

  console.log('The Maker smart agent is creating a leveraged CDP with the following parameters:');
  console.log(`Price Floor: $${minEthPrice}`);
  console.log(`Principal: ${ethToSend} ETH`);

  setTimeout(openCdp, 2000)

}

async function openCdp() {

  console.log("Opening a CDP...\n" + "Target ratio: 3.488765625")

  setTimeout(lockEth, 2000);

}

async function lockEth() {

  console.log("Locked 0.1 ETH\n" + "The agent drew 5 Dai")

  console.log('\n')

  //await cyberLoanCreator.createDAICollateralizedLoan(cdpData[1])

  await cyberLoanCreator.createDAICollateralizedLoan(5)

  return;

}

createCollateralizedLoan(pf, p);

module.exports = {

  createCollateralizedLoan

}
