var lendingManagerABI = require("./LendingManager.js");

var contractsAddresses = require('./contractsAddresses.js');
var accounts = require('./accounts.js');
var contractCaller = require('./contractCaller.js');

async function getContractInstance(network, providerType, contractName) {

  var web3, lendingManagerInstance;

  if (network == "local") {

    web3 = await contractCaller.getWeb3('local', providerType);

    if (contractName == "lendingManager") {

      try {

        lendingManagerInstance = await new web3.eth.Contract(

          lendingManagerABI.lendingManager.abi,
          contractsAddresses.localContracts[1],

          {from: accounts.local[0]}

        );

        return lendingManagerInstance;

      } catch(err) {

        return undefined;

      }

    }

  }

}

module.exports = {

  getContractInstance

}
